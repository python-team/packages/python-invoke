Source: python-invoke
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Antoine Beaupré <anarcat@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
Build-Depends-Indep:
 python3-alabaster,
 python3-icecream,
 python3-pytest,
 python3-pytest-relaxed,
 python3-releases,
 python3-yaml,
Rules-Requires-Root: no
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-invoke
Vcs-Git: https://salsa.debian.org/python-team/packages/python-invoke.git
Homepage: https://docs.pyinvoke.org

Package: python3-invoke
Architecture: all
Depends:
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-invoke-doc,
Description: Pythonic task execution - Python 3.x
 Invoke is a Python (2.6+ and 3.2+) task execution tool and library, drawing
 inspiration from various sources to arrive at a powerful and clean feature
 set.
 .
 Like Ruby's Rake tool and Invoke's own predecessor Fabric 1.x, it provides a
 clean, high level API for running shell commands and defining/organizing task
 functions from a tasks.py file.
 .
 From GNU Make, it inherits an emphasis on minimal boilerplate for common
 patterns and the ability to run multiple tasks in a single invocation.
 .
 Following the lead of most Unix CLI applications, it offers a traditional
 flag-based style of command-line parsing, deriving flag names and value types
 from task signatures.
 .
 Like many of its predecessors, it offers advanced features as well:
 namespacing, task aliasing, before/after hooks, parallel execution and more.
 .
 This package contains the Python 3.x module.
